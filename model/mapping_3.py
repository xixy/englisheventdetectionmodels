#coding=utf-8

import sys
import numpy as np
sys.path.append('/bert_as_feature/bert')
sys.path.append("..")
from event_types import *

def load_vocab(filename):
  '''
  从vocab文件中加载vocab
  Args:
    filename vocab文件路径
  Return:
    vocab: dict[word]=index
  '''
  vocab = {}
  idx = 0
  with open(filename) as f:
    for line in f:
      word = line.strip()
      vocab[word] = idx
      idx += 1
  return vocab


def get_bio_voc(tag_voc):
  BIO_voc = {}
  for i in tag_voc:
    if i[0] == 'B':
      BIO_voc[i] = 0
    if i[0] == 'I':
      BIO_voc[i] = 1
    if i[0] == 'O':
      BIO_voc[i] = 2
  return BIO_voc

def get_mapper_3(tag_voc,average=False):
  '''
  Args:
    tag_voc
  Return:
    map_np 67 * 3
  '''
  bio_voc = get_bio_voc(tag_voc)
  mapper = np.zeros((67, 3), dtype=np.float32)
  for x in tag_voc:
    mapper[tag_voc[x]][bio_voc[x]] = 1
  #3. 进行平均
  if average:
    mapper_list = mapper.tolist()
    for i in range(67):
      vec = mapper_list[i]
      new_index = vec.index(1)
      # 计算得到该小类别的数据
      counts = sum(mapper[:,new_index])
      vec = vec / counts
      mapper_list[i] = vec
      # print(mapper[:,new_index])
    mapper = np.array(mapper_list,dtype=np.float32)
  return mapper


if __name__ == '__main__':
  tag_vocab = load_vocab('../../data/original/tag_vocab.txt')
  mapper = get_mapper(tag_vocab,average = True)
  print(mapper[7])
  print(mapper[:,2])