#coding=utf-8

import sys
import numpy as np
sys.path.append('/bert_as_feature/bert')
sys.path.append("..")
from event_types import *

def load_vocab(filename):
	'''
	从vocab文件中加载vocab
	Args:
		filename vocab文件路径
	Return:
		vocab: dict[word]=index
	'''
	vocab = {}
	idx = 0
	with open(filename) as f:
		for line in f:
			word = line.strip()
			vocab[word] = idx
			idx += 1
	return vocab

def get_new_index(tag, tag_17_vocab):
	'''
	根据tag来获取tag_17_vocab中的index，例如
	B-Meet对应B-Contact，index为2
	'''
	if tag == 'O':
		return tag_17_vocab.index(tag)

	higher_type_index = None
	# 如果是BI形式，首先获取其对应的higher type
	for higher_type, lower_types in event_types.items():
		if tag[2:] in lower_types:
			higher_type_index = tag_17_vocab.index(tag[:2] + higher_type)
			return higher_type_index



def get_mapper_17(tag_vocab, average=False):
	'''
	'''
	# print event_types
	# 得到25_tag_vocab
	tag_17_vocab = []
	for higher_type in event_types:
		tag_17_vocab.append('B-'+higher_type)
		tag_17_vocab.append('I-'+higher_type)
	tag_17_vocab.append('O')
	# 进行sort
	tag_17_vocab.sort()

	# print tag_25_vocab
	#2. 构造映射关系
	# print(len(mapper[0]))
	# print(mapper)
	mapper = np.zeros((67, 17), dtype=np.float32)
	for tag, index in tag_vocab.items():
		new_index = get_new_index(tag, tag_17_vocab)
		mapper[index][new_index] = 1.0


	#3. 进行平均
	if average:
		mapper_list = mapper.tolist()
		for i in range(67):
			vec = mapper_list[i]
			new_index = vec.index(1)
			# 计算得到该小类别的数据
			counts = sum(mapper[:,new_index])
			vec = vec / counts
			mapper_list[i] = vec
			# print(mapper[:,new_index])
		mapper = np.array(mapper_list,dtype=np.float32)
	return mapper

if __name__ == '__main__':
	tag_vocab = load_vocab('../../data/original/tag_vocab.txt')
	mapper = get_mapper(tag_vocab,average = True)
	print(mapper[7])
	print(mapper[:,0])
